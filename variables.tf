variable members {
    type = number
}

variable prefix {
    type = string
}

variable publisher {
    type = string
}

variable image_name {
    type = string
}

variable image_sku {
    type = string
}

variable image_version {
    type = string 
}

variable custom_subscription {
    type = string 
}
