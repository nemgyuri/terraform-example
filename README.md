Terraform example
=======================

This example project demonstrates the automatic deployment of an Azure VM (size B1s) with terraform and cloud-init.

Prerequisites
--------------
- Azure-CLI, Terraform and Git installed
- A registered Azure account with active subscription 
- The subscription ID itself

How to use it
-------------
- Clone the repository via `git clone` or the GitLab web frontend.
- Login via Azure CLI onto your account, see [instructions here](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/guides/azure_cli)
- Replace the dummy `subscription_id` with yours in `terraform.tfvars` - can be obtained from the Azure CLI, see the link above.
- Swap out the SSH public key with your one, or you won't be able to login to the machine later. You need to replace the file `id_rsa.pub` with your public key, as well as the pubkey in `user-data.yml` in section users -> name: something -> ssh_authorized_keys.
- Modify `user-data.yml` according to your needs. 
- Deploy the VM onto Azure with `terraform init`, `terraform plan`, `terraform apply`. The usage of terraform commands isn't covered by this README.
