resource "azurerm_resource_group" "rg01" {
  name     = "${var.prefix}-rg"
  location = "westeurope"
  tags = {
    environment = "Bajnok-test"
  }
}

resource "azurerm_availability_set" "avset01" {
  name                = "${var.prefix}-avset-01"
  location            = azurerm_resource_group.rg01.location
  resource_group_name = azurerm_resource_group.rg01.name
  tags = {
    environment = "Bajnok-test"
  }
}

resource "azurerm_virtual_network" "vnet01" {
  name                = "${var.prefix}-vnet-01"
  resource_group_name = azurerm_resource_group.rg01.name
  location            = azurerm_resource_group.rg01.location
  address_space       = ["10.0.0.0/16"]
  tags = {
    environment = "Bajnok-test"
  }
}

resource "azurerm_subnet" "subnet01" {
  name                 = "${var.prefix}-subnet-01"
  resource_group_name  = azurerm_resource_group.rg01.name
  virtual_network_name = azurerm_virtual_network.vnet01.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_public_ip" "pub01" {
  name                = "${var.prefix}-pub-01"
  resource_group_name = azurerm_resource_group.rg01.name
  location            = azurerm_resource_group.rg01.location
  allocation_method   = "Static"
  sku                 = "Standard"
  tags = {
    environment = "Bajnok-test"
  }
}

resource "azurerm_network_security_group" "nsg01" {
  name                = "${var.prefix}-nsg-01"
  location            = azurerm_resource_group.rg01.location
  resource_group_name = azurerm_resource_group.rg01.name

  security_rule {
    name                       = "SSH"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

    security_rule {
    name                       = "IRC"
    priority                   = 100
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "6697"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    environment = "Bajnok-test"
  }
}

resource "azurerm_subnet_network_security_group_association" "nsgassoc01" {
  subnet_id                 = azurerm_subnet.subnet01.id
  network_security_group_id = azurerm_network_security_group.nsg01.id
}

resource "azurerm_network_interface" "nic" {
  count                = "${var.members}"
  name                 = "${var.prefix}-nic-0${count.index + 1}"
  location             = azurerm_resource_group.rg01.location
  resource_group_name  = azurerm_resource_group.rg01.name
  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.subnet01.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.pub01.id
  }
  tags = {
    environment = "Bajnok-test"
  }
}

data "local_file" "ud-01" {
  filename = "./user-data.yml"
}

resource "azurerm_linux_virtual_machine" "vm" {
  count               = "${var.members}" 
  name                = "${var.prefix}-vm-0${count.index + 1}"
  resource_group_name = azurerm_resource_group.rg01.name
  location            = azurerm_resource_group.rg01.location
  size                = "Standard_B1s"
  admin_username      = "adminuser"
  custom_data         = base64encode(data.local_file.ud-01.content)
  network_interface_ids = [
    element(azurerm_network_interface.nic.*.id, count.index + 1)
  ]
  admin_ssh_key {
    username   = "adminuser"
    public_key = file("./id_rsa.pub")
  }
  source_image_reference {
    publisher = "${var.publisher}"
    offer     = "${var.image_name}"
    sku       = "${var.image_sku}"
    version   = "${var.image_version}"
  }
  os_disk {
    storage_account_type = "Standard_LRS"
    caching              = "ReadWrite"
  }
  tags = {
    environment = "Bajnok-test"
  }

}